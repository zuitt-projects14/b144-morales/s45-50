//Mock database
//

const courseData = [
{
	id: "wdc001",
	name: "PHP - Laravel",
	description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur metus diam, consectetur ut leo euismod, ultrices tempus dolor.",
	price: 45000,
	onOffer: true
},
{
	id: "wdc002",
	name: "Phyton - Django",
	description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur metus diam, consectetur ut leo euismod, ultrices tempus dolor.",
	price: 50000,
	onOffer: true
},
{
	id: "wdc003",
	name: "Javascript - Springboot",
	description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur metus diam, consectetur ut leo euismod, ultrices tempus dolor.",
	price: 55000,
	onOffer: true
}
];

export default courseData;
